#include <stdio.h>
#include "../include/imageprocessing.h"

int main(int argc, char *argv[])
{
  char *opFile;
  if (argc == 1)
  {
    // Prints a red error message.
    printf("\033[0;31mPlease provide the input filename.\n");
    return 1;
  }
  if (argc == 3)
  {
    opFile = argv[2];
  }
  else
  {
    opFile = "op.bmp";
  }
  char *filename = argv[1];

  BITMAP_IMAGE *image = loadBitmap(filename);
  HSV_TRIPLET *hsv = createHSV(image, "sample.hsv");
  printImageInfo(image);
  BITMAP_IMAGE *gimage = createGrayScale(image, "gray.bmp");
  if (gimage == NULL)
    return 1;
  printImageInfo(gimage);
  printf("\nOutput\n");
  BITMAP_IMAGE *hist = histogramEqualization(gimage, "hist.bmp");
  BITMAP_IMAGE *op = createMonoChrome(hist, 150, opFile);
  BITMAP_IMAGE *h = createHueSatValImage(hist, 'h', hsv);
  BITMAP_IMAGE *s = createHueSatValImage(hist, 's', hsv);
  BITMAP_IMAGE *v = createHueSatValImage(hist, 'v', hsv);
  freeImage(h);
  freeImage(s);
  freeImage(v);
  freeImage(hist);
  freeImage(gimage);
  freeImage(op);
  freeImage(image);
  free(hsv);
  return 0;
}