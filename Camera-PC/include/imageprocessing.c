#include <stdio.h>
#include "imageprocessing.h"
#include <math.h>
#include <string.h>

/* 
 * getMonochrome takes a value
 * and creates a RGB_QUAD with
 * same r, g, and b values. 
 * Used for monochrome and 8bit Grayscale.
 */
RGB_QUAD getMonochrome(byte val)
{
  RGB_QUAD color;
  color.red = val;
  color.green = val;
  color.blue = val;
  color.reserved = 0;
  return color;
}

void freeImage(BITMAP_IMAGE *image)
{
  free(image->imageData);
  free(image);
}

BITMAP_IMAGE *loadBitmap(char *fileName)
{
  FILE *bmpFile;
  BITMAP_HEADER bmpHeader;
  BITMAP_INFO_HEADER bmpInfoHeader;
  byte *bitmapImage; //Image data
  BITMAP_IMAGE *bmpImage = (BITMAP_IMAGE *)malloc(sizeof(BITMAP_IMAGE));
  RGB_QUAD *colors;

  bmpFile = fopen(fileName, "rb");
  if (bmpFile == NULL)
  {
    printf("Unable to Open file.");
    return NULL;
  }

  fread(&bmpHeader, sizeof(BITMAP_HEADER), 1, bmpFile);

  if (bmpHeader.type != 0x4D42)
  { //0x4D42 - Magic Identifier for BMP, represents the characters 'B' and 'M'
    printf("Not a bmp.");
    free(bmpImage);
    fclose(bmpFile);
    return NULL;
  }

  fread(&bmpInfoHeader, sizeof(BITMAP_INFO_HEADER), 1, bmpFile);
  int nColors = 1 << bmpInfoHeader.bits;
  bmpInfoHeader.size = 124;
  int offset = bmpInfoHeader.size;
  if (bmpInfoHeader.bits < 16)
  {
    /*
    Images with less than 16-bit colors, require a
    RGB_QUAD array that defines all the colors. 
    Size of the array is 2^bits. (set in line 48)
    */
    colors = (RGB_QUAD *)malloc(nColors * sizeof(RGB_QUAD));

    fread(colors, sizeof(RGB_QUAD), nColors, bmpFile);
    offset += (sizeof(RGB_QUAD) * nColors);
    bmpHeader.offset = offset + 14;
  }

  BITMAP_INFO info;
  info.colors = colors;
  info.infoHeader = bmpInfoHeader;

  fseek(bmpFile, bmpHeader.offset, SEEK_SET);
  printf("offset: %d\n", bmpHeader.offset);
  bitmapImage = (byte *)malloc(bmpHeader.size - bmpHeader.offset);

  if (!bitmapImage)
  {
    printf("Failed to allocate memory for bitmap image");
    free(bitmapImage);
    free(colors);
    fclose(bmpFile);
    return NULL;
  }

  int bitsize = bmpHeader.size - bmpHeader.offset;

  int n = fread(bitmapImage, 1, bitsize, bmpFile);

  if (bitmapImage == NULL)
  {
    printf("Failed to read bitmap image");
    free(bitmapImage);
    free(colors);
    fclose(bmpFile);
    return NULL;
  }

  bmpImage->bmpHeader = bmpHeader;
  bmpImage->bmpInfo = info;
  bmpImage->imageData = bitmapImage;
  printf("%d\n", sizeof(BITMAP_INFO_HEADER));
  fclose(bmpFile);
  return bmpImage;
}

BITMAP_IMAGE *createGrayScale(BITMAP_IMAGE *image, char *fileName)
{
  FILE *outputBmp = fopen(fileName, "wb");
  if (outputBmp == NULL)
  {
    printf("Unable to Open file.");
    return NULL;
  }
  BITMAP_HEADER *bmHeader;
  BITMAP_INFO_HEADER *bmInfoHeader;
  BITMAP_INFO bmInfo;
  BITMAP_INFO_HEADER *currentInfoHeader = &image->bmpInfo.infoHeader;
  BITMAP_HEADER *currentHeader = &image->bmpHeader;
  int initialBitsize = image->bmpHeader.size - image->bmpHeader.offset;

  if (image->bmpInfo.infoHeader.bits != 24)
  {
    printf("\033[0;31mPlease use a 24-bit image\n");
    return NULL;
  }

  RGB_QUAD colors[256];
  for (int i = 0; i < 256; i++)
  {
    colors[i] = getMonochrome(i);
  }

  memcpy(&bmHeader, &currentHeader, sizeof(currentHeader));
  memcpy(&bmInfoHeader, &currentInfoHeader, sizeof(currentInfoHeader));

  bmInfoHeader->bits = 8;
  bmInfoHeader->nColors = 256;

  int totalSize = bmInfoHeader->height * bmInfoHeader->width;
  int totalSizeWithHeader = totalSize + 138 + (256 * 4);

  bmInfoHeader->imageSize = totalSize;
  bmHeader->size = totalSizeWithHeader;
  bmHeader->offset = 138 + 256 * 4;
  bmInfo.colors = &colors;
  bmInfo.infoHeader = *bmInfoHeader;
  int rowSize = ((bmInfoHeader->width + 31) / 32) * 4;

  fwrite(bmHeader, sizeof(BITMAP_HEADER), 1, outputBmp);
  fwrite(bmInfoHeader, sizeof(BITMAP_INFO_HEADER), 1, outputBmp);
  fwrite(colors, sizeof(RGB_QUAD), bmInfoHeader->nColors, outputBmp);

  int bitsize = bmHeader->size - bmHeader->offset;
  BITMAP_INFO_HEADER *infoHeader = &image->bmpInfo.infoHeader;
  byte *bwImageData = (byte *)malloc(bitsize);

  for (int i = 0, j = 0; i < initialBitsize && j < bitsize; i += 3, j++)
  {
    // Grayscale value according to Linear Approximation of perceptual luminance
    byte bwValue = 0.114 * image->imageData[i] +
                   0.587 * image->imageData[i + 1] +
                   0.299 * image->imageData[i + 2];
    bwImageData[j] = bwValue; //B
  }

  fwrite(bwImageData, bitsize, 1, outputBmp);
  fclose(outputBmp);
  BITMAP_IMAGE *resultImage = (BITMAP_IMAGE *)malloc(sizeof(BITMAP_IMAGE));

  resultImage->bmpHeader = *bmHeader;
  resultImage->bmpInfo = bmInfo;
  resultImage->imageData = bwImageData;
  printf("\033[0;32mGrayscale Conversion Complete.\n\033[0m");
  return resultImage;
}

void printImageInfo(BITMAP_IMAGE *image)
{
  printf("Offset  : %d\n", (*image).bmpHeader.offset);
  printf("Height  : %d\n", (*image).bmpInfo.infoHeader.height);
  printf("Width   : %d\n", (*image).bmpInfo.infoHeader.width);
  printf("HSize   : %d\n", (*image).bmpHeader.size);
  printf("Bits    : %d\n", (*image).bmpInfo.infoHeader.bits);
  printf("ISize   : %d\n", (*image).bmpInfo.infoHeader.imageSize);
  printf("Size    : %d\n", (*image).bmpInfo.infoHeader.size);
  printf("Comp    : %d\n", (*image).bmpInfo.infoHeader.compression);
  printf("nColor  : %d\n", (*image).bmpInfo.infoHeader.nColors);
  printf("impCol  : %d\n", (*image).bmpInfo.infoHeader.importantColors);
}

BITMAP_IMAGE *createMonoChrome(BITMAP_IMAGE *image, byte threshold, char *fileName)
{
  BITMAP_HEADER *bmHeader;
  BITMAP_INFO_HEADER *bmInfoHeader;
  BITMAP_INFO bmInfo;
  BITMAP_INFO_HEADER *currentInfoHeader = &image->bmpInfo.infoHeader;
  BITMAP_HEADER *currentHeader = &image->bmpHeader;
  byte *monochromeImageData;
  RGB_QUAD white = getMonochrome(255);
  RGB_QUAD black = getMonochrome(0);
  RGB_QUAD colors[2] = {black, white};
  FILE *outputBmp = fopen(fileName, "wb");
  int originalBitsize = image->bmpHeader.size - image->bmpHeader.offset;
  if (outputBmp == NULL)
  {
    printf("Unable to Open file.");
    fclose(outputBmp);
    return NULL;
  }

  if (image->bmpInfo.infoHeader.bits != 8)
  {
    printf("\033[0;31mPlease use a 8-bit image.\n");
    fclose(outputBmp);
    return NULL;
  }

  memcpy(&bmHeader, &currentHeader, sizeof(currentHeader));
  memcpy(&bmInfoHeader, &currentInfoHeader, sizeof(currentInfoHeader));

  bmInfoHeader->bits = 1;
  bmInfoHeader->nColors = 2;
  bmInfoHeader->planes = 1;

  int totalSize = (bmInfoHeader->height * bmInfoHeader->width) / 8;
  int totalSizeWithHeader = totalSize + 138;

  bmInfoHeader->imageSize = totalSize;
  bmHeader->size = totalSizeWithHeader;
  bmHeader->offset = 146;
  bmInfo.infoHeader = *bmInfoHeader;
  bmInfo.colors = &colors;
  int rowSize = ((bmInfoHeader->width + 31) / 32) * 4;

  fwrite(bmHeader, sizeof(BITMAP_HEADER), 1, outputBmp);
  fwrite(bmInfoHeader, sizeof(BITMAP_INFO_HEADER), 1, outputBmp);
  fwrite(colors, sizeof(RGB_QUAD), bmInfoHeader->nColors, outputBmp);

  monochromeImageData = (byte *)malloc(rowSize * bmInfoHeader->height);
  printf("Row %d   %d\n", rowSize, bmInfoHeader->imageSize);
  for (int h = 0; h < bmInfoHeader->height; h++)
  {
    for (int row = 0; row < rowSize; row++)
    {
      byte currentByte = 0;
      for (int bit = 0; bit < 8; bit++)
      {
        int currIndex = (h)*bmInfoHeader->width + row * 8 + bit;
        byte bwValue = image->imageData[currIndex];
        if (bwValue > threshold)
        {
          currentByte += (1 << (7 - bit));
        }
      }
      monochromeImageData[h * rowSize + row] = currentByte;
    }
  }

  printf("\033[0;32mMonochrome Conversion Complete.\n\033[0m");
  fwrite(monochromeImageData, rowSize * bmInfoHeader->height, 1, outputBmp);
  fclose(outputBmp);
  BITMAP_IMAGE *resultImage = (BITMAP_IMAGE *)malloc(sizeof(BITMAP_IMAGE));

  resultImage->bmpHeader = *bmHeader;
  resultImage->bmpInfo = bmInfo;
  resultImage->imageData = monochromeImageData;
  return resultImage;
}

float max(float a, float b)
{
  return a > b ? a : b;
}

float min(float a, float b)
{
  return a > b ? b : a;
}

/*
 * Calculate Hue Saturation and Value from R G B values.
 */
HSV_TRIPLET calculateHSV(float r, float g, float b)
{
  float hue, saturation, value, colorMax, colorMin, diff, h, s, v;
  HSV_TRIPLET triplet;
  r = r / 255.0;
  g = g / 255.0;
  b = b / 255.0;
  colorMax = max(max(r, g), b);
  colorMin = min(min(r, g), b);
  diff = colorMax - colorMin;

  if (colorMax == colorMin)
  {
    h = 0;
  }
  else if (colorMax == r)
  {
    h = fmod(60 * ((g - b) / diff) + 360, 360.0);
  }
  else if (colorMax == g)
  {
    h = fmod(60 * ((b - r) / diff) + 120, 360.0);
  }
  else if (colorMax == b)
  {
    h = fmod(60 * ((r - g) / diff) + 240, 360.0);
  }

  if (colorMax == 0)
  {
    s = 0;
  }
  else
  {
    s = (diff / colorMax);
  }

  v = colorMax;

  triplet.hue = h;
  triplet.saturation = s;
  triplet.value = v;
  return triplet;
}

HSV_TRIPLET *createHSV(BITMAP_IMAGE *image, char *fileName)
{
  HSV_TRIPLET *resultHSV;
  FILE *outputHSV = fopen(fileName, "w");
  int size = image->bmpHeader.size - image->bmpHeader.offset;
  resultHSV = (HSV_TRIPLET *)malloc(size * sizeof(HSV_TRIPLET));
  char *str[80];
  sprintf(str, "%d\n", size / 3);
  fwrite(str, strlen(str), 1, outputHSV);
  for (int i = 0, j = 0; i < size && j < size / 3; i += 3, j++)
  {
    HSV_TRIPLET trip = calculateHSV((float)image->imageData[i], (float)image->imageData[i + 1], (float)image->imageData[i + 2]);
    resultHSV[j] = trip;
    sprintf(str, "%f,%f,%f\n", trip.hue, trip.saturation, trip.value);
    fwrite(str, strlen(str), 1, outputHSV);
  }
  fclose(outputHSV);
  return resultHSV;
}

// Convert a value from 0-360 to 0-255
int hueToRGB(float hue)
{
  if (hue == 0)
    return 0;
  return floor((hue * 255) / 360);
}

BITMAP_IMAGE *createHueSatValImage(BITMAP_IMAGE *image, char required, HSV_TRIPLET *hsv)
{
  char *fileName[20];
  sprintf(fileName, "%c.bmp", required);
  FILE *outputBmp = fopen(fileName, "wb");
  if (outputBmp == NULL)
  {
    printf("Unable to Open file.");
    fclose(outputBmp);
    return NULL;
  }
  BITMAP_HEADER *bmHeader;
  BITMAP_INFO_HEADER *bmInfoHeader;
  BITMAP_INFO bmInfo;
  BITMAP_INFO_HEADER *currentInfoHeader = &image->bmpInfo.infoHeader;
  BITMAP_HEADER *currentHeader = &image->bmpHeader;
  int initialBitsize = image->bmpHeader.size - image->bmpHeader.offset;

  RGB_QUAD colors[256];
  for (int i = 0; i < 256; i++)
  {
    colors[i] = getMonochrome(i);
  }

  memcpy(&bmHeader, &currentHeader, sizeof(currentHeader));
  memcpy(&bmInfoHeader, &currentInfoHeader, sizeof(currentInfoHeader));

  bmInfoHeader->bits = 8;
  bmInfoHeader->nColors = 256;

  int totalSize = bmInfoHeader->height * bmInfoHeader->width;
  int totalSizeWithHeader = totalSize + 138 + (256 * 4);

  bmInfoHeader->imageSize = totalSize;
  bmHeader->size = totalSizeWithHeader;
  bmHeader->offset = 138 + 256 * 4;
  bmInfo.colors = &colors;
  bmInfo.infoHeader = *bmInfoHeader;
  int rowSize = ((bmInfoHeader->width + 31) / 32) * 4;

  fwrite(bmHeader, sizeof(BITMAP_HEADER), 1, outputBmp);
  fwrite(bmInfoHeader, sizeof(BITMAP_INFO_HEADER), 1, outputBmp);
  fwrite(colors, sizeof(RGB_QUAD), bmInfoHeader->nColors, outputBmp);

  int bitsize = bmHeader->size - bmHeader->offset;
  BITMAP_INFO_HEADER *infoHeader = &image->bmpInfo.infoHeader;
  byte *hsvImageData = (byte *)malloc(bitsize);

  if (required == 'h')
  {
    for (int i = 0; i < bitsize; i++)
    {
      hsvImageData[i] = hueToRGB(hsv[i].hue);
    }
  }
  else if (required == 's')
  {
    for (int i = 0; i < bitsize; i++)
    {
      hsvImageData[i] = hsv[i].saturation * 255;
    }
  }
  else
  {
    for (int i = 0; i < bitsize; i++)
    {
      hsvImageData[i] = hsv[i].value * 255;
    }
  }

  fwrite(hsvImageData, bitsize, 1, outputBmp);
  fclose(outputBmp);
  BITMAP_IMAGE *resultImage = (BITMAP_IMAGE *)malloc(sizeof(BITMAP_IMAGE));

  resultImage->bmpHeader = *bmHeader;
  resultImage->bmpInfo = bmInfo;
  resultImage->imageData = hsvImageData;
  printf("\033[0;32mGrayscale Conversion Complete.\n\033[0m");
  return resultImage;
}

BITMAP_IMAGE *histogramEqualization(BITMAP_IMAGE *image, char *fileName)
{
  FILE *outputBmp = fopen(fileName, "wb");
  BITMAP_HEADER bmHeader = image->bmpHeader;
  BITMAP_INFO_HEADER bmInfoHeader = image->bmpInfo.infoHeader;
  if (outputBmp == NULL)
  {
    printf("Unable to Open file.");
    fclose(outputBmp);
    return NULL;
  }

  RGB_QUAD colors[256];
  for (int i = 0; i < 256; i++)
  {
    colors[i] = getMonochrome(i);
  }

  fwrite(&bmHeader, sizeof(BITMAP_HEADER), 1, outputBmp);
  fwrite(&bmInfoHeader, sizeof(BITMAP_INFO_HEADER), 1, outputBmp);
  fwrite(colors, sizeof(RGB_QUAD), image->bmpInfo.infoHeader.nColors, outputBmp);

  int histogram[256];
  byte grayLevels[256];

  for (int i = 0; i < 256; i++)
  {
    histogram[i] = 0;
    grayLevels[i] = 0;
  }

  int bitsize = bmHeader.size - bmHeader.offset;
  byte *bwImageData = (byte *)malloc(bitsize);
  memcpy(bwImageData, image->imageData, bitsize);

  for (int i = 0; i < bitsize; i++)
  {
    histogram[bwImageData[i]] += 1;
  }

  int minFreq = bitsize + 1;
  int freq = 0;
  int cd[256] = {0};
  for (int i = 0; i < 256; i++)
  {
    freq += histogram[i];
    cd[i] = freq;
    minFreq = (int)min(freq, minFreq);
  }

  for (int i = 0; i < 256; i++)
  {
    grayLevels[i] = round((((float)cd[i] - minFreq) * 255) / (bitsize - minFreq));
  }

  for (int i = 0; i < bitsize; i++)
  {
    bwImageData[i] = grayLevels[bwImageData[i]];
  }

  fwrite(bwImageData, bitsize, 1, outputBmp);
  fclose(outputBmp);
  memcpy(image->imageData, bwImageData, bitsize);
  printf("\033[0;32mHistogram Equalization Complete.\n\033[0m");
  return image;
}
