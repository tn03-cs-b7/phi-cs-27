#ifndef IMAGE_PROCESSING_HEADER
#define IMAGE_PROCESSING_HEADER

typedef unsigned char byte;

/*
 *General Information about the BMP Image
 */
#pragma pack(push, 1)
typedef struct
{
  unsigned short type;
  unsigned int size;
  unsigned short int reserved1;
  unsigned short int reserved2;
  unsigned int offset;
} BITMAP_HEADER;
#pragma pack(pop)

/* RGB Colors for bitmap palette definition
 *  This is used mainly for images with 
 *  1, 2, 4, or 8-bit pixels. Images above
 *  24 bit depth don't require a color
 *  palette.
 */
typedef struct
{
  byte blue;
  byte green;
  byte red;
  byte reserved;
} RGB_QUAD;

/*
 * HSV Triplet for storing
 * Hue, Saturation and Value.
 */
typedef struct
{
  float hue;
  float saturation;
  float value;

} HSV_TRIPLET;

/*
 * Detailed Information about the BMP Image
 * V5
 */
#pragma pack(push, 1)
typedef struct
{
  unsigned int size;
  int width;
  int height;
  unsigned short planes;
  unsigned short bits;
  unsigned int compression;
  unsigned int imageSize;
  int xPixelsPerMeter;
  int yPixelsPerMeter;
  unsigned int nColors;
  unsigned int importantColors;
  unsigned int redMask;
  unsigned int greenMask;
  unsigned int blueMask;
  unsigned int alphaMask;
  unsigned int csType;
  unsigned int redX;
  unsigned int redY;
  unsigned int redZ;
  unsigned int greenX;
  unsigned int greenY;
  unsigned int greenZ;
  unsigned int blueX;
  unsigned int blueY;
  unsigned int blueZ;
  unsigned int gammaRed;
  unsigned int gammaGreen;
  unsigned int gammaBlue;
  unsigned int intent;
  unsigned int iccProfile;
  unsigned int iccSize;
  unsigned int reserved;
} BITMAP_INFO_HEADER;
#pragma pack(pop)

typedef struct
{
  BITMAP_INFO_HEADER infoHeader;
  RGB_QUAD *colors; // To support 1,2,4 and 8 bit images
} BITMAP_INFO;

/*
 *Represents a BMP Image with headers.
 */
typedef struct
{
  BITMAP_HEADER bmpHeader;
  BITMAP_INFO bmpInfo;
  byte *imageData;
} BITMAP_IMAGE;

/*
 * Function: printImageInfo
 * ----------------------------
 *  DisplayImageInfo
 *
 *   image: BITMAP_IMAGE
 *
 */
void printImageInfo(BITMAP_IMAGE *image);

/*
 * Function: loadBitmap
 * ----------------------------
 *  Load a bitmap image from disk.
 *
 *   fileName: file name of image.
 * returns: A BITMAP_IMAGE
 */
BITMAP_IMAGE *loadBitmap(char *filename);

/*
 * Function: createGrayScale
 * ----------------------------
 *  Convert the bitmap to gray scale and
 *  save new bitmap image to disk. The new
 *  bitmap will be an 8bit bitmap.
 *  This functions requires a 24bit Bitmap.
 *
 *   image: BITMAP_IMAGE
 *   fileName: file name of image.
 * returns: A BITMAP_IMAGE
 */
BITMAP_IMAGE *createGrayScale(BITMAP_IMAGE *image, char *fileName);

/*
 * Function: histogramEqualization
 * ----------------------------
 *  Apply Histogram equalization on a
 *  8-bit image and save the image.
 *
 *   image: BITMAP_IMAGE
 * returns: A BITMAP_IMAGE
 */
BITMAP_IMAGE *histogramEqualization(BITMAP_IMAGE *image, char *fileName);

/*
 * Function: createMonoChrome
 * ----------------------------
 *  Convert a bitmap to monochrome(1-bit) using
 *  thresholding technique and save the new 
 *  bitmap image to disk. The input image 
 *  must be an 8-bit bitmap image.
 *
 *   image: BITMAP_IMAGE
 *   threshold: (Byte) If grayscale value is greater than
 *              than threshold, monochrome pixel 
 *              will be 1 else 0.
 *   fileName: file name of image.
 *
 */
BITMAP_IMAGE *createMonoChrome(BITMAP_IMAGE *image, byte threshold, char *fileName);

/*
 * Function: createHueSatValImage
 * ----------------------------
 *  Get the Hue, Saturation or Value images
 *  from a HSV_TRIPLET array.
 *
 *   image: BITMAP_IMAGE
 *   char: 'h' for Hue Image
 *         's' for Saturation Image
 *         'v' for Value Image
 *   hsv: Pointer to HSV_TRIPLET array.
 *
 */
BITMAP_IMAGE *createHueSatValImage(BITMAP_IMAGE *image, char required, HSV_TRIPLET *hsv);

/*
 * Function: calculateHSV
 * ----------------------------
 *  Calculates the Hue, Saturation and Value
 *  from RGB values.
 *
 *   r: Red value
 *   g: Green value
 *   b: Blue value
 * 
 *  returns: HSV_TRIPLET
 */
HSV_TRIPLET calculateHSV(float r, float g, float b);

/*
 * Function: createHSV
 * ----------------------------
 *  From an RGB based bitmap
 *  generate HSV values.
 *
 *   image: BITMAP_IMAGE
 *   fileName: file name to save HSV data.
 *  returns: Pointer to an array of HSV_TRIPLET
 */
HSV_TRIPLET *createHSV(BITMAP_IMAGE *image, char *fileName);

/*
 * Function: freeImage
 * ----------------------------
 *  Free a BITMAP_IMAGE
 *
 *   image: BITMAP_IMAGE
 */
void freeImage(BITMAP_IMAGE *image);

#endif