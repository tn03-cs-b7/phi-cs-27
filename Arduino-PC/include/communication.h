#ifndef COMMUNICATION_HEADER
#define COMMUNICATION_HEADER

#include <stdbool.h>

typedef struct
{
  unsigned char STX;    //Start
  unsigned char STATUS; //Status code of the receiver that will help to know if data is received or not
  unsigned char SIZE;   //A byte to represent the length of data

} DATA_PACKET_HEADER; //Struct representing the packet header

typedef struct
{
  unsigned char CHECKSUM; //Checksum For error detection
  unsigned char ETX;      //End

} DATA_PACKET_TRAILER; //Struct representing the packet trailer

/*
 * Function: validateChecksum
 * ----------------------------
 *  Validates the data using checksum.
 *
 *   data: data bytes
 *   checksum: Checksum value
 *   packetHeader: Packet header
 *
 *   returns: true if the data and header are error-less.
 */
bool validateChecksum(unsigned char *data, DATA_PACKET_HEADER *packetHeader);

/*
 * Function: generateChecksum
 * ----------------------------
 *  Generates a checksum for data and header.
 *
 *   data: data bytes
 *   packetHeader: Packet header
 *
 *   returns: the checksum generated.
 */
unsigned char generateChecksum(unsigned char *data, DATA_PACKET_HEADER *packetHeader);

/*
 * Function: sendPacket
 * ----------------------------
 *  Send a packet.
 *
 *   data: data bytes
 *   packetHeader: Packet header
 *   packetTrailer: Packet trailer
 *
 */
void sendPacket(unsigned char *data, DATA_PACKET_HEADER *packetHeader, DATA_PACKET_TRAILER *packetTrailer);

/*
 * Function: receivePacket
 * ----------------------------
 *  Receive a packet. 
 *
 *   data: data bytes
 *   packetHeader: Packet header
 *   packetTrailer: Packet trailer
 *
 */
void receivePacket(unsigned char *data, DATA_PACKET_HEADER *packetHeader, DATA_PACKET_TRAILER *packetTrailer);

/*
 * Function: createPacketHeader
 * ----------------------------
 *  Creates a DATA_PACKET_HEADER
 *
 *   status: Status code of the receiver that will 
 *           help to know if data is received or not
 *   size: A byte to represent the length of data
 *  
 * returns: pointer to the created DATA_PACKET_HEADER 
 */
DATA_PACKET_HEADER *createPacketHeader(unsigned char status, unsigned char size);

/*
 * Function: createPacketTrailer
 * ----------------------------
 *  Creates a DATA_PACKET_TRAILER
 *
 *   checksum: For error detection
 *  
 * returns: pointer to the created DATA_PACKET_TRAILER
 */
DATA_PACKET_TRAILER *createPacketTrailer(unsigned char checksum);

/*
 * Function: createMotorDirectionPayload
 * ----------------------------
 *  Creates the data payload to set the motor
 *  direction
 *
 *   isClockwise: true if direction is clockwise else false.
 *  
 * returns: data payload.
 */
unsigned char *createMotorDirectionPayload(bool isClockwise);

/*
 * Function: createMotorSpeedPayload
 * ----------------------------
 *  Creates the data payload to set the motor
 *  speed
 *
 *   speed: Speed value (0-255)
 *  
 * returns: data payload.
 */
unsigned char *createMotorSpeedPayload(unsigned char speed);

/*
 * Function: createLaserLinePayload
 * ----------------------------
 *  Controls the Laser Line Generator
 *
 *   turnOn: true to turn-on and false to turn-off
 *  
 * returns: data payload.
 */
unsigned char *createLaserLinePayload(bool turnOn);

/*
 * Function: parseDataPayload
 * ----------------------------
 *  Parse the data payload.
 *
 *   data: data payload.
 *  
 */
void parseDataPayload(unsigned char *data);

#endif