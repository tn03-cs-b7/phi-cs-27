#include "communication.h"
#include <stdio.h>
#include <stdlib.h>

/*
 * Function: generateChecksum
 * ----------------------------
 *  Generates a checksum for data and header.
 *
 *   data: data bytes
 *   packetHeader: Packet header
 *
 *   returns: the checksum generated.
 */
unsigned char generateChecksum(unsigned char *data, DATA_PACKET_HEADER *packetHeader)
{
  unsigned char checksum = 0;
  unsigned char dataSize = (*packetHeader).SIZE;
  checksum += (*packetHeader).STX;
  checksum += (*packetHeader).STATUS;
  checksum += (*packetHeader).SIZE;

  for (int i = 0; i < dataSize; i++)
  {
    checksum += data[i];
  }
  checksum ^= 0xFF;

  return checksum;
}

/*
 * Function: validateChecksum
 * ----------------------------
 *  Validates the data using checksum.
 *
 *   data: data bytes
 *   checksum: Checksum value
 *   packetHeader: Packet header
 *
 *   returns: true if the data and header are error-less.
 */
bool validateChecksum(unsigned char *data, DATA_PACKET_HEADER *packetHeader)
{
  unsigned char checksum = generateChecksum(data, packetHeader);
  if (checksum != 0xFF)
  {
    return false;
  }

  return true;
}

/*
 * Function: createPacketHeader
 * ----------------------------
 *  Creates a DATA_PACKET_HEADER
 *
 *   status: Status code of the receiver that will 
 *           help to know if data is received or not
 *   size: A byte to represent the length of data
 *  
 * returns: pointer to the created DATA_PACKET_HEADER 
 */
DATA_PACKET_HEADER *createPacketHeader(unsigned char status, unsigned char size)
{
  DATA_PACKET_HEADER *packet = malloc(sizeof(DATA_PACKET_HEADER));
  (*packet).STATUS = status;
  (*packet).SIZE = size;
  (*packet).STX = 0x02;

  return packet;
}

/*
 * Function: createPacketTrailer
 * ----------------------------
 *  Creates a DATA_PACKET_TRAILER
 *
 *   checksum: For error detection
 *  
 * returns: pointer to the created DATA_PACKET_TRAILER
 */
DATA_PACKET_TRAILER *createPacketTrailer(unsigned char checksum)
{
  DATA_PACKET_TRAILER *packet = malloc(sizeof(DATA_PACKET_TRAILER));
  (*packet).ETX = 0x03;
  (*packet).CHECKSUM = checksum;

  return packet;
}

/*
 * Function: createMotorDirectionPayload
 * ----------------------------
 *  Creates the data payload to set the motor
 *  direction
 *
 *   isClockwise: true if direction is clockwise else false.
 *  
 * returns: data payload.
 */
unsigned char *createMotorDirectionPayload(bool isClockwise)
{
  unsigned char *payload = (unsigned char *)malloc(2 * sizeof(unsigned char));
  payload[0] = 0x0D; //Command
  if (isClockwise)
  { //Data
    payload[1] = 0x00;
  }
  else
  {
    payload[1] = 0x01;
  }

  return payload;
}

/*
 * Function: createMotorSpeedPayload
 * ----------------------------
 *  Creates the data payload to set the motor
 *  speed
 *
 *   speed: Speed value (0-255)
 *  
 * returns: data payload.
 */
unsigned char *createMotorSpeedPayload(unsigned char speed)
{
  if (speed < 0x00 || speed > 0xFF)
    return NULL;

  unsigned char *payload = (unsigned char *)malloc(2 * sizeof(unsigned char));
  payload[0] = 0x0C; //Command
  payload[1] = speed;

  return payload;
}

/*
 * Function: createLaserLinePayload
 * ----------------------------
 *  Controls the Laser Line Generator
 *
 *   turnOn: true to turn-on and false to turn-off
 *  
 * returns: data payload.
 */
unsigned char *createLaserLinePayload(bool turnOn)
{
  unsigned char *payload = (unsigned char *)malloc(2 * sizeof(unsigned char));
  payload[0] = 0x0A; //Command
  if (turnOn)
  { //Data
    payload[1] = 0x01;
  }
  else
  {
    payload[0] = 0x00;
  }

  return payload;
}

/*
 * Function: parseDataPayload
 * ----------------------------
 *  Parse the data payload.
 *
 *   data: data payload.
 *  
 */
void parseDataPayload(unsigned char *data);